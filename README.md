# EFP Lutris Installer

EFP (Escape From Pripyat) is a modpack creaded by Maid and Haruka for [STALKER Anomaly](https://www.moddb.com/mods/stalker-anomaly). For more information, additional mods, support and guides [join EFP Discord](https://discord.gg/efp).

This installer lets you run the whole modpack on Linux through Lutris without much fiddling. **Make sure you're using Lutris version 0.5.17 or later**, otherwise this installer will not work.

Remember, the modpack is quite big, 38.1 GB unpacked. The download and extraction process will take a while, so just be patient and let the installer do it's thing.

## Benefits of this installer over manual installation

- **No need to launch vanilla Anomaly once before running MO2.** Required files (AnomalyLauncher.cfg and commandline.txt) are automatically created. They're empty, but that is all that is required to bypass first launch. 
- **No need to setup a portable MO2 instance.** ModOrganizer.ini config file in this repo will be automatically downloaded. Since wineprefix directory structure is always predictable, there's no need to modify the config to specify MO2 and managed game directories (C:\EFP and C:\MO2, which correspond to ~/Games/stalker-anomaly-efp/drive_c/EFP and ~/Games/stalker-anomaly-efp/drive_c/MO2).
- **No annoying popup in MO2 on first launch asking about registration and nxm.** The installer generates an nxmhandler.ini config file that suppresses that popup. 
- **No need to manually pick EFP 4.2 profile in MO2.** This too is handled by just importing the ModOrganizer.ini from this repo.
- **Most required files are downloaded automatically.** The only exceptions are [EFPv4-2_EFP.7z](https://drive.google.com/file/d/1geyrKt-ZaFOfZqJTa7aKSK18Omc_gARq/view) and [EFPv4-2_MO2.7z](https://drive.google.com/file/d/1YKH9m-uFnq6MgQihrLVYwJ9ASCBI3ea4/view) because Google Drive doesn't like direct downloads anymore. You will be prompted to download these archives during install.
- **Correct wine version (lutris-7.2-2-x86_64) is automatically set.**
